Technologies:
"browser-sync": "^2.27.7",
"gulp": "^4.0.2",
"del": "^6.0.0",
"sass": "^1.49.0",
"gulp-autoprefixer": "^8.0.0",
"gulp-clean": "^0.4.0",
"gulp-clean-css": "^4.3.0",
"gulp-concat": "^2.6.1",
"gulp-imagemin": "^7.1.0",
"gulp-js-minify": "^0.0.3",
"gulp-sass": "^5.1.0",
"gulp-uglify": "^3.0.2"
-----------------------
Каждая ветка - это отдельный раздел, также последние ветки - это адаптив
Верстки делали по принципу Mobile First
